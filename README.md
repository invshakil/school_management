It was a 2-person team project development using OOP PHP. Where School will have student, teacher, admin, parent’s different dashboard. Student can check class lecture, class notice, class test notices from their different subject teacher. They can check examination routine and result also. Where parents can check class routine, class attendance, result etc.

Online attendance for student by teacher. Class notice, lecture upload by teachers. Also admin can manage all of these things from their secured admin panel. 

We completed 70% of this projects. 

* Different type of user dashboard.
* Class routine, class attendance, class notice, examination routine, result etc.
* Front end for school where school information will be shown.